import Swiper, { Navigation, Pagination,Scrollbar,Autoplay } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

Swiper.use([Navigation,Scrollbar,Autoplay]);
document.addEventListener("alpine:init", () => {
    console.log('alpine swiperjs init');
    Alpine.data(
        "swiperjs",
        (options) => ({
            instance:null,
            init() {
                this.instance = new Swiper(this.$el,options);
            }
        })
    );
});