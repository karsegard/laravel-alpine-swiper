@props([
    'config'=>['slidesPerView'=>3],
    'class'=>"max-w-full w-full overflow-hidden ",
    'wrapperAttributes'=>[]
])

@php
    $wrapperAttributes = new \Illuminate\View\ComponentAttributeBag($wrapperAttributes);
@endphp
<div x-cloak x-data="swiperjs(@js($config))" {{ $attributes->merge(['class'=>$class]) }}>
    <!-- Additional required wrapper -->
    {{ $before ?? '' }}


    <div  {{$wrapperAttributes->merge(['class'=>'swiper-wrapper'])}}>
        <!-- Slides -->
        {{$slot}}
       
    </div>
    {{ $after ?? '' }}
</div>