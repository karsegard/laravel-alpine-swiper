<?php

namespace KDA\Laravel\Alpine\Swiper;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Alpine\Swiper\Facades\Swiper as Facade;
use KDA\Laravel\Alpine\Swiper\Swiper as Library;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasAssets;
use KDA\Laravel\Layouts\Facades\LayoutManager;
use KDA\Laravel\Traits\HasViews;
use KDA\Laravel\PackageMix\Facades\AssetManager;
use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Traits\HasManifest;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasAssets;
    use HasConfig;
    use HasViews;
    use HasManifest;
    protected $packageName = 'alpine-swiper';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    protected $configDir = 'config';
    protected $configs = [
        'kda/alpine-swiper.php'  => 'kda.alpine-swiper'
    ];
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }
    //called after the trait were booted
    protected function bootSelf()
    {
       /* LayoutManager::registerScripts([
            'swiper' => __DIR__.'/../assets/js/swiper.js',
        ]);*/
        AssetManager::registerVendor('alpine-swiper',__DIR__.'/..');
        LayoutManager::registerRenderHook(
            'alpine.plugins',
            function(){
               return  Blade::render('<script defer src="{{vendor_mix(\'alpine-swiper\',\'/assets/js/swiper.js\')}}"></script>');
            }
        );
    }
}
