<?php

namespace KDA\Laravel\Alpine\Swiper\Facades;

use Illuminate\Support\Facades\Facade;

class Swiper extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
