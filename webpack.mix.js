let mix = require("laravel-mix");

mix
.setPublicPath('./')
.js("resources/js/swiper.js", "assets/js")
.version();